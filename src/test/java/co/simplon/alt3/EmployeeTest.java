package co.simplon.alt3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import co.simplon.alt3.exceptions.NegativeSalaryException;

public class EmployeeTest {
    @Test
    void testSetSalaryNominal() {
        Employee e = new Employee();
        try {
            e.setSalary(1500);
            assertEquals(1500, e.getSalary());
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }
    @Test
    void testSetSalaryNegatif() {
        Employee e = new Employee();
        assertThrows(NegativeSalaryException.class, () -> { e.setSalary(-1500); });
    }
}
