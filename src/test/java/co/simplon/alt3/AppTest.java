package co.simplon.alt3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        App app = new App();
        assertTrue( !app.estMajeur(17) );
    }

    @Test 
    public void testAjouter2entiersPositifs() {
        App app = new App();
        assertEquals(2, app.ajouter(1, 1));
    }
    @Test 
    public void testAjouter2entiersPositifscas2() {
        App app = new App();
        assertEquals(3, app.ajouter(1, 2));
    }
    @Test
    public void testAjouter1entierPositif1entierNegatif() {
        App app = new App();
        assertEquals(-1, app.ajouter(1, -2));
    }
    @Test
    public void testAjouter2entiersNegatifs() {
        App app = new App();
        assertEquals(-3, app.ajouter(-1, -2));
    }
}
