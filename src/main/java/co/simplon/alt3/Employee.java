package co.simplon.alt3;

import co.simplon.alt3.exceptions.NegativeSalaryException;

public class Employee {
    private int salary;

    public Employee() {
        
    }

    public Employee(int salary) throws NegativeSalaryException {
        this.setSalary(salary);
    }

    /**
     * @return the salary
     */
    public int getSalary() {
        return salary;
    }

    /**
     * @param salary the salary to set
     */
    public void setSalary(int salary) throws NegativeSalaryException {
        if (salary < 0) {
            throw new NegativeSalaryException();
        }        
        this.salary = salary;
    }

}
