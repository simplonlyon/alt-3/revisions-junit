package co.simplon.alt3.exceptions;

public class NegativeSalaryException extends Exception {
    public NegativeSalaryException(String msg) {
        super(msg);
    }
    public NegativeSalaryException() {
        super("Le salaire ne peut pas être négatif");
    }
}
